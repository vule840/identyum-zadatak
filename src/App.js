import "./assets/scss/app.scss";
import { ContextApiProvider } from "./store/context-api";
import React from "react";
import Layout from "./components/Layout";

function App() {
  return (
    <div className="App">
      <ContextApiProvider>
        <Layout />
      </ContextApiProvider>
    </div>
  );
}

export default App;
