import React, { useContext } from "react";
import ContextApi from "../store/context-api";

const GeneratedForm = (props) => {
  const ctx = useContext(ContextApi);

/*   let optioninp = [];
  if (props.options) {
    props.options
      .filter((gender) => gender.value !== "other" && gender.value !== "female")
      .map((option) => {
        optioninp = option.value;
      });
  } */
  return (
    <>
      <label htmlFor={props.label}>{props.label}</label>
      <input
        keys={props.name}
        onChange={ctx.lastFormChange(props.name)}
        required={props.required}
        type={props.type}
        name={props.name}
        maxLength={props.maxlength}
        minLength={props.minlength}
      />
    </>
  );
};

export default GeneratedForm;
