import React from "react";
import '../assets/scss/app.scss'

const SvgLoader = () => {
  return (
    <div className="c-svg-wrapper">
      <svg className="svg" width="100" height="100">
        <circle cx="50" cy="50" r="25" />
      </svg>
    </div>
  );
};

export default SvgLoader;
