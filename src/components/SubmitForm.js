import React, { useContext } from "react";
import ContextApi from "../store/context-api";
import GeneratedForm from "./GeneratedForm";
import SvgLoader from "./SvgLoader"
const SubmitForm = () => {
  const ctx = useContext(ContextApi);
  console.log(ctx);
  const refreshPage = ()=>{
    window.location.reload();
 }
  return (
    <React.Fragment>
      <h4 className="u-text-center">Generated form for submit</h4>
      <fieldset disabled={ctx.finalMessage}>
        <form>
          {ctx.formToGenerate.elements &&
            ctx.formToGenerate.elements.length > 0 &&
            ctx.formToGenerate.elements
              .filter((input) => input.name !== "iban")
              .map((input) => {
              
                return (
                  <>
                    <GeneratedForm
                      options={input.options}
                      label={input.label}
                      type={input.type}
                      name={input.name}
                      maxLength={input.maxlength}
                      minLength={input.minlength}
                    />
                  </>
                );
              })}

          <input onClick={ctx.finalSubmit} type="submit" value="Submit" />
        </form>
      </fieldset>
      {ctx.loader2 && <SvgLoader />}       
      {ctx.finalMessage && (
        <>
          <h2 className="u-text-center c-sucess">Uspješno završen proces</h2>
          <br></br>
         {/*  <form onSubmit={window.location.reload()}>
            <input className="c-button c-button--primary u-text-center u-mx-auto u-block" type="submit" value="Edit"/>
          </form> */}
          <button
            className="c-button c-button--primary u-text-center u-mx-auto u-block"
            onClick={refreshPage}
          >
            Edit
          </button> 
        </>
      )}
    </React.Fragment>
  );
};

export default SubmitForm;
