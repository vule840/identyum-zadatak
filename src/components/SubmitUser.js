import React, { useState, useContext, useEffect } from "react";
import "../assets/scss/app.scss";
import ContextApi from "../store/context-api";
import { AuthApi } from "../store/context-api";
import SvgLoader from "./SvgLoader";
import axios from "axios";


export default function SubmitUser() {
  const ctx = useContext(ContextApi);
  const authCtx = useContext(AuthApi);

  const [verf, showVerf] = useState(false);
  const [checked, setChecked] = useState("SMS");
  const [counter, setCounter] = useState(0);



  const handleChange = (e) => {
    setChecked(e.target.value);


    if (checked === "SMS") {
      showVerf(true);
      axios
        .get(process.env.REACT_APP_LOGIN + "/api/v3/select/email", {
          headers: {
            "X-SESSION-UUID": localStorage.getItem("session")
          },
        })
        .then((response) => {
          setCounter(response.data.submitTimeout);
        
        })
        .catch(function (error) {
          alert("Something went wrong 🥺");
        });
    } else {
      showVerf(false);
    }
  };
  useEffect(() => {
    if(localStorage.getItem("reload") === 'true'){
      ctx.reset()
      localStorage.removeItem("reload");
    } 
    counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
 
  }, [counter,ctx,checked]);


  //val check, empty
  // const enteredPassIsValid = authCtx.password.trim() !== ''
  const passInputIsInvalid = !authCtx.passIsValid && authCtx.passTouched;
  const userInputIsInvalid = !authCtx.userIsValid && authCtx.userTouched;
  const emailInputIsInvalid = !authCtx.emailIsValid && authCtx.emailTouched;
  //
  const passInputClasses = passInputIsInvalid ? "form form--invalid" : "form";
  //const userInputClasses = userInputIsInvalid ? 'form form--invalid' : 'form'
  const emailInputClasses = emailInputIsInvalid ? "form form--invalid" : "form";

  //choose method
  let methodChoose;
  if (ctx.loginMethods) {
    if (ctx.loginMethods.length > 0) {
      methodChoose = ctx.loginMethods.map((method, index) => {
        return (
          <>
            <input
              type="radio"
              value={method}
              id={method}
              onChange={handleChange}
              checked={ctx.loginMethods[index] === checked}
              key={method}
            />
            <label htmlFor="{method}">{method}</label>
          </>
        );
      });
    }
  }

  return (
    <div>
      <h4 className="u-text-center">Login user</h4>
      <fieldset disabled={ctx.disabledForm}>
        <form className={passInputClasses}>
          <label htmlFor="password">Password</label>
          <input
            onChange={authCtx.passVal}
            onBlur={authCtx.blurVal}
            placeholder="password"
            type="password"
          />

          {passInputIsInvalid && (
            <p className="error">Password must not be empty</p>
          )}
          <label htmlFor="username">Username</label>
          <input
            onChange={authCtx.userVal}
            onBlur={authCtx.userBlurVal}
            placeholder="username"
            type="text"
          />
          {userInputIsInvalid && (
            <p className="error">Name must not be empty</p>
          )}
          <input onClick={ctx.showTokenSubmit} type="submit" value="Submit" />
        </form>
      </fieldset>
      {ctx.loader && <SvgLoader />}

      {ctx.loginMethods && ctx.loginMethods.length > 0 && ctx.verfMet && (
        <div>
          <h4 className="u-text-center">Choose a verification method</h4>
          <form className="c-form-radio ">{methodChoose}</form>
        </div>
      )}

      {verf && ctx.loginMethods && (
        <form className={emailInputClasses} onSubmit={authCtx.emailVal}>
          <h4 className="u-pt-2 u-text-center">Submit your email</h4>

          <input
            onChange={authCtx.emailChange}
            onBlur={authCtx.emailBlur}
            type="email"
            id="email"
            name="email"
          />
          {emailInputIsInvalid && (
            <p className="error">Email must not be empty</p>
          )}
          <p className="c-counter">Timer: {counter}</p>
          <input type="submit" value="Submit" />
        </form>
      )}
    </div>
  );
}
