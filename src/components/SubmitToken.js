import React, { useContext } from "react";
import ContextApi from "../store/context-api";
import { AuthApi } from "../store/context-api";

export default function SubmitToken() {
  const ctx = useContext(ContextApi);
  const authCtx = useContext(AuthApi);
  return (
    <React.Fragment>
      <h4 className="u-text-center">Submit token</h4>
      <form action="">
        <input onChange={authCtx.tokenVal} placeholder="token" type="text" />
        <input onClick={ctx.showPinSubmit} type="submit" value="Submit" />
      </form>
    </React.Fragment>
  );
}
