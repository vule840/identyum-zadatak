import React, { useContext } from "react";
import ContextApi from "../store/context-api";
import { AuthApi } from "../store/context-api";
const SubmitPin = () => {
  const ctx = useContext(ContextApi);
  const authCtx = useContext(AuthApi);
  return (
    <React.Fragment>
      {!ctx.pinAlrdyDefined ? (
        <>
          <h4 className="u-text-center">Submit Pin</h4>
          <form action="">
            <label htmlFor="pin">Pin</label>
            <input
              onChange={authCtx.pinVal}
              placeholder="pin"
              type="password"
            />
            <input
              onChange={authCtx.pinValRepeated}
              placeholder="pin repeated"
              type="password"
            />
            <input onClick={ctx.showFormSubmit} type="submit" value="Submit" />
          </form>
        </>
      ) : (
        <>
          <h4 className="u-text-center">Pin already defined</h4>
          <p className="u-text-center">Submit it to generate form</p>
          <form>
            <input
              onChange={ctx.pinChange}
              placeholder="pin submit"
              type="password"
            />
            <input onClick={ctx.pinSubmitLast} type="submit" value="Submit" />
          </form>
        </>
      )}
    </React.Fragment>
  );
};

export default SubmitPin;
