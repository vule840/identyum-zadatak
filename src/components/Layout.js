import React, { useContext } from "react";
import SubmitUser from "./SubmitUser";
import SubmitToken from "./SubmitToken";
import SubmitPin from "./SubmitPin";
import SubmitForm from './SubmitForm'
import ContextApi from "../store/context-api";
import SvgLoader from "./SvgLoader"
const Layout = () => {
  const ctx = useContext(ContextApi);


  return (
    <div className="o-wrap">
 
       {ctx.loader && ctx.user && <SvgLoader/>}
      {!ctx.user && <SubmitUser />} 
      {ctx.token && <SubmitToken />}
      {ctx.pin && <SubmitPin />}
      {ctx.form &&<SubmitForm/>}
    </div>
  );
};

export default Layout;
