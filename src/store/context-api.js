import React, { useState } from "react";
import axios from "axios";
//import useInput from "../hooks/useInput";

const ContextApi = React.createContext({
  loader: false,
  loader2: false,
  user: true,
  token: false,
  pin: false,
  form: false,
  disabledForm: false,
  verfMet: false,
  loginMethods: [],
  pinAlrdyDefined: false,
  formToGenerate: [],
  finalMessage: "",
  lastFormValues: [],
  verReset: false,
  showTokenSubmit: (token) => {},
  showPinSubmit: (pin) => {},
  showFormSubmit: (form) => {},
  pinSubmitLast: () => {},
  pinChange: () => {},
  finalSubmit: () => {},
  lastFormChange: () => {},
  reset: () => {},
});

export const AuthApi = React.createContext({
  password: "",
  username: "",
  emailSubmited: "",
  tokenSubmit: "",
  pinSubmit: "",
  pinSubmitRepeat: "",

  passIsValid: false,
  userIsValid: false,
  passTouched: false,
  userTouched: false,
  emailTouched: false,
  session: "",
  blurVal: () => {},
  userBlurVal: () => {},
  emailBlur: () => {},

  passVal: () => {},
  userVal: () => {},
  emailVal: () => {},
  tokenVal: () => {},
  emailChange: () => {},
  checkEmail: () => {},
  pinVal: () => {},
  pinValRepeated: () => {},
});

export const ContextApiProvider = (props) => {
  /**
   * STATES
   */

  //SHOW/HIDE
  const [user, showUser] = useState(false);
  const [loader, showLoader] = useState(false);
  const [loader2, showLoader2] = useState(false);
  const [token, showToken] = useState(false);
  const [pin, showPin] = useState(false);
  const [form, showForm] = useState(false);
  const [verfMet, showVerfM] = useState(false);
  //const [verReset, showVerReset] = useState(false);
  //SHOW/HIDE SUBCOMPONENTS

  //USER EMAIL LOGIN
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [emailSubmited, emailSubmit] = useState("");
  const [tokenSubmit, setToken] = useState("");

  //VAL
  const [passTouched, setPassTouched] = useState(false);
  const [userTouched, setUserTouched] = useState(false);
  const [emailTouched, setEmailTouched] = useState(false);
  const [passIsValid, setPassValid] = useState(true);
  const [userIsValid, setUserValid] = useState(true);
  const [emailIsValid, setEmailValid] = useState(true);
  const [loginMethods, addLoginMethods] = useState([]);

  //SESSION
  //const [session, setSession] = useState("");
 // const [bearer, setBearer] = useState("");
  //PIN
  const [pinSubmit, setPin] = useState("");
  const [pinSubmitRepeat, setPinRepeat] = useState("");
  const [pinLast, lastPinSubmit] = useState("");
  const [pinAlrdyDefined, pinDefined] = useState("");
  //FORM
  const [disabledForm, disableForm] = useState(false);
  const [formToGenerate, setGeneratedForm] = useState("");
  //LAST FORM
  const [finalMessage, showFinalMessage] = useState("");

  const entries = {
    first_name: "first_name",
    last_name: "last_name",
    sex: "male",
    date_of_birth: "01.07.1987",
  };

  const [lastFormValues, changeFormValues] = React.useState({
    [entries.first_name]: "first_name",
    [entries.last_name]: "last_name",
    [entries.sex]: "male",
    [entries.date_of_birth]: "01.07.1987",
  });
  /*********
   *
   * HANDLERS
   *
   *********/

  //ONCHANGE handlers

  const passValHandler = (e) => {
    setPassword(e.target.value);
    if (e.target.value.trim() !== "") {
      setPassValid(true);
    }
  };

  const userValHandler = (e) => {
    setUsername(e.target.value);
    if (e.target.value.trim() !== "") {
      setUserValid(true);
    }
  };
  const emailChangeHandler = (e) => {
    emailSubmit(e.target.value);
    if (e.target.value.trim() !== "") {
      setEmailValid(true);
    }
  };
  const tokenChangeHandler = (e) => {
    setToken(e.target.value);
    /* if (e.target.value.trim() !== "") {
      setEmailValid(true);
    } */
  };

  const pinChangeHandler = (e) => {
    setPin(e.target.value);
  };
  const pinChangeReaptedHandler = (e) => {
    setPinRepeat(e.target.value);
    if (pinSubmit !== pinSubmitRepeat) {
    }
  };

  const pinChangeSubmitHandler = (e) => {
    lastPinSubmit(e.target.value);
  };

  const handleInputChange = (fieldName) => (e) => {
    const fieldValue = e.target.value;

    changeFormValues((prevState) => ({
      ...prevState,
      [fieldName]: fieldValue,
    }));
  };

  /*   const firstNameHandler = (e) => {
    setFirstName(e.target.value);
  };
  const lastNameHandler = (e) => {
    setLastName(e.target.value);
  };
  const genderHandler = (e) => {
    setGender(e.target.value);
  };
  const birthHandler = (e) => {
    setDateBirth(e.target.value);
  }; */
  /**BLUR */

  const blurValHandler = () => {
    setPassTouched(true);
    if (password.trim() === "") {
      setPassValid(false);
    }
  };
  const userBlurValHandler = () => {
    setUserTouched(true);
    if (username.trim() === "") {
      setUserValid(false);
    }
  };

  const emailBlurVal = (e) => {
    setEmailTouched(true);
    if (e.target.value.trim() === "") {
      setEmailValid(false);
    }
  };

  /********
   *
   * 1.USER
   *  1.1 Send user
   *  1.2 Get verf Methods
   * */

  const setUserSubmit = (e) => {
    e.preventDefault();

    setPassTouched(true);
    setUserTouched(true);

    if (password.trim() === "") {
      /*  setPassValid(false); */
      alert("Password is empty");
      return;
    }
    if (password.length > 15 || password.length < 15) {
      /*  setPassValid(false); */
      alert("Password must exactly have 15 characters");
      return;
    }

    if (username.trim() === "") {
      alert("Username is empty");
      return;
    }
    if (username.length > 13 || username.length < 13) {
      /*  setPassValid(false); */
      alert("Username must exactly have 13 characters");
      return;
    }
    setUserValid(true);
    setPassValid(true);

    axios
      .post(process.env.REACT_APP_USER + "/v1/auth/password", {
        password: password,
        username: username,
      })
      .then((response) => {
        showLoader(true);

        setTimeout(() => {
          disableForm(true);
          showLoader(false);
          showVerfM(true);
        }, 1500);

        const sessionState = response.data.session_state;
        //setSession(sessionState);
        localStorage.setItem("session", sessionState)
        return axios.get(process.env.REACT_APP_LOGIN + "/api/v3/start", {
          headers: {
            "X-SESSION-UUID": localStorage.getItem("session"),
          },
        });
      })
      .then((response) => {
        addLoginMethods(response.data.availableLoginMethods);
      })
      .catch(function (error) {
        alert("Something went wrong 🥺 ..., wrong creds #️⃣*️⃣ maybe?");

        return;
      });
  };

  /******************
   *
   * 1.3 USER  EMAIL SUBMIT
   *
   ***************/

  const emailValHandler = (e) => {
    e.preventDefault();

    setEmailTouched(true);
    //emailSubmit(e.target[0].value);
    if (emailSubmited.trim() === "") {
      alert("Email is empty");
      return;
    }
    setEmailValid(true);

    showUser(!user);
    showLoader(true);
    setTimeout(() => {
      showLoader(false);

      showToken(true);
    }, 500);

    axios({
      method: "post",
      url: process.env.REACT_APP_LOGIN + "/api/v3/email/submit",
      headers: {
        "X-SESSION-UUID": localStorage.getItem("session"),
      },
      data: {
        email: emailSubmited,
        language: "hr",
      },
    })
      .then((response) => {})
      .catch(function (error) {
        alert("Something went wrong 🥺");

        return;
      });
  };

  /*******
   *
   * 2.TOKEN - show pin
   *   2.1 /email/verify
   *  3.1 /start - (obratiti pozornost na ‘pinDefined’
   * */

  const showPinHandler = (e) => {
    e.preventDefault();
    if (tokenSubmit.trim() === "") {
      alert("Token cant be empty...");
      return;
    }
    if (tokenSubmit.length > 6 || tokenSubmit.length < 6) {
      alert("Token must have exactly 6 characters");
      return;
    }

    axios({
      method: "post",
      url: process.env.REACT_APP_LOGIN + "/api/v3/email/verify",
      headers: {
        "X-SESSION-UUID": localStorage.getItem("session"),
      },
      data: {
        otp: tokenSubmit,
      },
    })
      .then((response) => {
        showToken(false);

        showLoader(true);
        setTimeout(() => {
          showLoader(false);
          showPin(true);
        }, 1500);
        const Bearer = response.data.accessToken.access_token;
        localStorage.setItem("Bearer", Bearer);

        //setBearer(Bearer);

        return axios.get(process.env.REACT_APP_PIN + "/v2/start", {
          headers: {
            Authorization: "Bearer " + localStorage.getItem("Bearer"),
          },
        });
      })
      .then((response) => {
        const pinAlreadyDefined = response.data.pinDefined;
        pinDefined(pinAlreadyDefined);
      })
      .catch(function (error) {
        alert("Something went wrong 🥺, I guess its wrong token?");

        return;
      });
  };
  /*******
   *
   * 3.PIN - show form
   *
   *   3.2 /set ili submit??
   *    3.3
   * */

  const ShowFormHandler = () => {
    if (pinSubmit.trim() === "") {
      alert("Pin cant be empty...");
      return;
    }
    if (pinSubmitRepeat.trim() === "") {
      alert("Repeated pin cant be empty...");
      return;
    }
    showLoader(true);
    setTimeout(() => {
      showLoader(false);
    }, 1500);

    axios({
      method: "post",
      url: process.env.REACT_APP_PIN + "/v2/set",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("Bearer"),
      },
      data: {
        language: "hr",
        pin: pinSubmit,
        repeatedPin: pinSubmitRepeat,
      },
    })
      .then((response) => {
        //pinSumbiting(true);
      })
      .catch(function (error) {
        alert("Something went wrong 🥺");

        return;
      });
  };

  /*******
   *
   * 4.FORM
   *  3.3 submit pin
   *  4,1 generate form
   * */
  const pinSubmitHandler = (e) => {
    e.preventDefault();
    if (pinLast.trim() === "") {
      alert("Your submited pin cant be empty...");
      return;
    }
    if (pinLast.length < 6 || pinLast.length > 6) {
      alert("Your pin needs to have exactly 6 characters");
      return;
    }

    axios({
      method: "post",
      url: process.env.REACT_APP_PIN + "/v2/submit",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("Bearer"),
      },
      data: {
        pin: pinLast,
      },
    })
      .then((response) => {
        showLoader(true);
        showPin(false);
        setTimeout(() => {
          showForm(true);
          showLoader(false);
        }, 1500);
        return axios
          .get(process.env.REACT_APP_FORM + "/v2/start", {
            headers: {
              Authorization: "Bearer " + localStorage.getItem("Bearer"),
            },
          })
          .then((response) => {
            const generatedForm = response.data;
            setGeneratedForm(generatedForm);
          })
          .catch(function (error) {
            alert("Something went wrong 🥺");

            return;
          });
      })
      .catch(function (error) {
        alert("Something went wrong 🥺, wrong pin maybe 🔢?");

        return;
      });
  };

  /*******
   *
   * 4.FINAL FORM
   * 4.2 - submit final form, show message
   *
   *********/

  const finalSubmitHandler = (e) => {
    e.preventDefault();
    console.log(lastFormValues);
    if (lastFormValues.first_name.trim() === "") {
      alert("Your name must not be empty ");
      return;
    }
    if (lastFormValues.last_name === "") {
      alert("Your last name must not be empty ");
      return;
    }
    if (lastFormValues.date_of_birth === "") {
      alert("Your Date of birth must not be empty ");
      return;
    }

    axios({
      method: "post",
      url: process.env.REACT_APP_FORM + "/v2/submit",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("Bearer"),
      },
      data: {
        entries: [
          {
            key: "first_name",
            value: lastFormValues.first_name,
          },
          {
            key: "last_name",
            value: lastFormValues.last_name,
          },
          {
            key: "sex",
            value: "male",
          },
          {
            key: "date_of_birth",
            value: lastFormValues.date_of_birth,
          },
        ],
      },
    })
      .then((response) => {
     
   /*      localStorage.setItem("hideLogin", 'true');
        localStorage.setItem("showVerf", 'true'); */
        localStorage.setItem("reload", 'true');
        showLoader2(true);
        setTimeout(() => {
          showLoader2(false);
          showFinalMessage(true);
        }, 1500);
      })
      .catch(function (error) {
        alert("Something went wrong 🥺");

        return;
      });
  };

  const resetHandler = () => {
    //window.location.reload();
    localStorage.removeItem("Bearer");
    //disableForm(true);
    //showVerReset(true);
    axios
    .post(process.env.REACT_APP_USER + "/v1/auth/password", {
      password: '#interview_013$',
      username: 'interview_013',
    })
    .then((response) => {
      showLoader(true);

      setTimeout(() => {
        disableForm(true);
        showLoader(false);
        showVerfM(true);
      }, 1500);

      const sessionState = response.data.session_state;
      //setSession(sessionState);
   
      //setSession(sessionState);
      localStorage.setItem("session", sessionState)

      return axios.get(process.env.REACT_APP_LOGIN + "/api/v3/start", {
        headers: {
          "X-SESSION-UUID": localStorage.getItem("session"),
        },
      });
    })
    .then((response) => {
      addLoginMethods(response.data.availableLoginMethods);
    })
    .catch(function (error) {
      alert("Something went wrong 🥺 ..., wrong creds #️⃣*️⃣ maybe?");

      return;
    });
  };

  return (
    <AuthApi.Provider
      value={{
        password: password,
        username: username,
        emailSubmited: emailSubmited,
        tokenSubmit: tokenSubmit,
        pinSubmit: pinSubmit,
        pinSubmitRepeat: pinSubmitRepeat,
        //USER
        //pass
        passIsValid: passIsValid,
        passTouched: passTouched,
        passVal: passValHandler,
        userVal: userValHandler,
        blurVal: blurValHandler,
        //user
        userIsValid: userIsValid,
        userTouched: userTouched,
        //userVal: userValHandler,
        userBlurVal: userBlurValHandler,
        //EMAIL
        emailTouched: emailTouched,
        emailIsValid: emailIsValid,
        emailVal: emailValHandler,
        emailBlur: emailBlurVal,
        emailChange: emailChangeHandler,

        //SESSION
        //session: session,
        //TOKEN
        tokenVal: tokenChangeHandler,
        pinVal: pinChangeHandler,
        pinValRepeated: pinChangeReaptedHandler,
      }}
    >
      <ContextApi.Provider
        value={{
          loader: loader,
          loader2: loader2,
          user: user,
          token: token,
          pin: pin,
          form: form,
          verfMet: verfMet,
          //verReset: verReset,
          disabledForm: disabledForm,
          loginMethods: loginMethods,
          formToGenerate: formToGenerate,
          pinAlrdyDefined: pinAlrdyDefined,
          finalMessage: finalMessage,
          lastFormValues: lastFormValues,
          showTokenSubmit: setUserSubmit,
          showPinSubmit: showPinHandler,
          showFormSubmit: ShowFormHandler,
          pinSubmitLast: pinSubmitHandler,
          pinChange: pinChangeSubmitHandler,
          finalSubmit: finalSubmitHandler,
          lastFormChange: handleInputChange,
          reset: resetHandler,
        }}
      >
        {props.children}
      </ContextApi.Provider>
    </AuthApi.Provider>
  );
};

export default ContextApi;
